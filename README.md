# Description 
This project uses YOLO v2 architecture as a Classification network for trained Faces. This architecture is designed using Keras library and trained for known Home users faces.

It makes use of Histogram of Oriented Gradients to extract additional Features for Face Shapes and preventing false positive alarms.
Haar's Cascade is used in image processing to classify between a photo image and real face, in addition with eye blinks as a secondary parameter.

The project makes use of Arduino Nano for integrating the data obtained from IR and proximity sensor, turning on IR LED's when there is low illumination, and interrupting the Raspberry Pi Processor when there is detection. It uses FreeRTOS for pre-emptive taskSwitching and to enable low power modes.
