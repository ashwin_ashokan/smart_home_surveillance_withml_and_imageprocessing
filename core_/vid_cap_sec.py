import numpy as np
import cv2
import os
import time
import datetime



def main(num):
        
        cap = cv2.VideoCapture(0)
        seconds_duration = num
        now = datetime.datetime.now()
        finish_time = now + datetime.timedelta(seconds=seconds_duration)
        name1=now.strftime("%d")
        name2=now.strftime("%B")
        name3=now.strftime("%y")
        name4=now.strftime("%I")
        name5=now.strftime("%M")
        name6=now.strftime("%S")
        name=name1+name2+name3+"_"+name4+"_"+name5+"_"+name6
        print (name)
        a=0     
             # Define the codec and create VideoWriter object
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(name +'.avi',fourcc, 20.0, (640,480))
        out2 = cv2.VideoWriter(name +'tl.avi',fourcc, 20.0, (640,480))



             
        while(datetime.datetime.now() < finish_time):
                inow = datetime.datetime.now()
                iname1=inow.strftime("%d")
                iname2=inow.strftime("%B")
                iname3=inow.strftime("%y")
                iname4=inow.strftime("%I")
                iname5=inow.strftime("%M")
                iname6=inow.strftime("%S")
                iname=iname1+iname2+iname3+"_"+iname4+":"+iname5+":"+iname6
                ret, frame = cap.read()
                if ret==True:
                    frame = cv2.flip(frame,1)


                    

                    
                    fontface = cv2.FONT_HERSHEY_SIMPLEX
                    fontscale = 1
                    fontcolor = (0, 0, 0)
                    x = 210 #position of text
                    y = 420 #position of text
                    cv2.putText(frame,iname, (x,y), fontface, fontscale, fontcolor,2) 
                    
                    # write the flipped frame
                    out.write(frame)


            
                    cv2.imshow('frame',frame)
                    a=a+1
                    if a==3:
                            a=0
                            # write the flipped frame
                            out2.write(frame)

                    cv2.imshow('frame',frame)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                else:
                    break
             # Release everything if job is finished

             
        cap.release()
        out.release()
        cv2.destroyAllWindows()

        return name 




if __name__ == '__main__':
    a=main(10)
    print (a)
            
    
